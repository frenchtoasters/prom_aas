FROM alpine:latest

# Copy Configurations
COPY /configuration /maas/configuration

# Install dependices
RUN apk add --no-cache wget curl

# Install Helm
RUN mkdir -p /maas/helm

WORKDIR /maas/helm

RUN wget https://storage.googleapis.com/kubernetes-helm/helm-v2.12.3-linux-amd64.tar.gz
RUN tar -zxvf helm-v2.12.3-linux-amd64.tar.gz
RUN mv linux-amd64/helm /usr/local/bin/helm

# Install Kubectl
RUN mkdir -p /maas/kubectl

WORKDIR /maas/kubectl

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin

# Set EntryPoint Path
WORKDIR /maas
